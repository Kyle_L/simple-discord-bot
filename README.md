## Simple Discord Bot
This is code for a simple Discord bot that says a random line when the keyword is said in chat.

## Version
This is version 1.0

## Compatibility
This is built and compatible with version 3.6 of Python.

## Projects Included
Includes...

* discordbot.py
* sampleinput.txt

## License
Copyright Kyle Lierer (2019)