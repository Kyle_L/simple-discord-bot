# Works with Python 3.6
import sys
import discord
import os
import random

# The token of the Discord bot.
TOKEN = 'xxxxxxxxx'

# When a message starts with this, the bot will respond.
KEY_WORD = '!bot'

# Creates the discord client.
client = discord.Client()

# Prompts the user with the file path where
# the discord bot will pull outputs.
file_path = input("File path: ")

if not os.path.isfile(file_path):
    print(file_path + ' is not valid')
    print('exiting program')
    sys.exit(1)

# Prompts the user with the key word for
# which the bot responds to
key_word = '!' + input("Key word: !")

# Load all lines to <lines> var and strip them of "new line" char \n
with open(file_path) as f:
    lines = [line.rstrip('\n') for line in f]

# Called when a new message appears in the discord server.
@client.event
async def on_message(message):
    # We do not want the bot to reply to itself
    if message.author == client.user:
        return

    # Checks to see if the message starts with the KEY_WORD.
    if message.content.startswith(key_word):
        # Chooses a random line from the file.
        rnd_line = random.choice(lines)
        msg = rnd_line.format(message)

        # Sends the message.
        await client.send_message(message.channel, msg)

@client.event
async def on_ready():
    print('------')
    print('Logged in')
    print('Username: ' + client.user.name)
    print('UserID: ' + client.user.id)
    print('Key word is ' + key_word)
    print('Pulling output from ' + file_path)
    print('------')

client.run(TOKEN)